import unittest
import sys
sys.path.append('C:\\IMQ\\')
from SERVER import server

class ServerTesting(unittest.TestCase):
    def test_create_message(self):
        file_name='127.0.0.1_52222'
        address=('127.0.0.1', 52222)
        server_obj=server.Server()
        filename=server_obj.create_message(address,'hello')
        self.assertEqual(filename,file_name)

if __name__ == '__main__':
    unittest.main()
    