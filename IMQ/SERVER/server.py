import socket
import time
import sqlite3
import sys
sys.path.append("C:\\IMQ\\")
from DATABASE import db_connection
from datetime import date
from _thread import *
from SERVER import constants

class Server:
    def __init__(self):
        print("Welcome to the Server")

    def create_message(self,address,reply):
        filename=((address[0]+"_"+str(address[1])))
        client_name=filename
        currentTime = time.ctime(time.time()) + "\r\n"
        today_date = str(date.today())
        db_connect=db_connection.Database('imq_server2')
        db_connect.entry_to_database(client_name,currentTime,today_date,reply)
        return filename 
        
    def run_server(self,ThreadCount):
        while True:
            Client, address = ServerSocket.accept()
            print('Connected to: ' + address[0] + ':' + str(address[1]))
            start_new_thread(server.connect_to_client, (Client, address))
            ThreadCount += 1
            print('Thread Number: ' + str(ThreadCount))
        ServerSocket.close()
        
    def connect_to_client(self,Client,address):
        while True:
            try:
                data = Client.recv(2048)
                reply = data.decode('utf-8')
                print(reply)
                if reply == "Subscriber101":
                    element =server.messages_parser()
                    Client.sendall(str.encode(element[2]))
                    exit
                else:
                    client_file_name = server.create_message(address, reply)
                    Client.sendall(str.encode(reply))
                if not bool(data):
                    print(client_file_name + "Has Exited")
                    break
            except:
                print("Client Has Exited")
                break      
        Client.close()
                
    def connect_to_socket(self,host,port,max_connections,ThreadCount):
        try:
            ServerSocket.bind((host, port))
            print('Waiting for a Connection..')
            ServerSocket.listen(max_connections)
            server.run_server(ThreadCount)
        except:
            print("Unable to Connect to socket")

    def messages_into_queue(self):
        database_object=db_connection.Database("imq_server2")
        Queue=database_object.getting_messages_from_database()
        return Queue

    def messages_parser(self):
        queue=server.messages_into_queue()
        elements = [a_tuple[0] for a_tuple in queue ]
        values = [i for i in elements if i]
        return values

if __name__=='__main__':
    server=Server()
    server.messages_into_queue()
    ServerSocket = socket.socket()
    server.connect_to_socket(constants.HOST,constants.PORT,constants.MAX_CONNECTIONS,constants.THREAD_COUNT)