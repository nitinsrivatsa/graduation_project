import sqlite3
from datetime import *
from six.moves import *
from configparser import *
import socket
import time
import json
from datetime import date
from _thread import *

class Database:
    def __init__(self,db_name):
        self.db_name=db_name
    
    def entry_to_database(self,client_name,time,today_date,data):        
        try:
            db_connection = sqlite3.connect(self.db_name)
            cursor_object = db_connection.cursor()
            query = ''' INSERT INTO root2 (name_of_client) values(?) '''
            query1 = ''' INSERT INTO client_data2 (time,date,client_id,client_data) values(?,?,?,?) '''
            cursor_object.execute(query,(client_name,))
            cursor_object.execute(query1,(time,today_date,client_name,data))
            db_connection.commit()
            db_connection.close()
        except:
            print("Database could not be connected")

    def getting_messages_from_database(self):
        try:
            db_connection = sqlite3.connect(self.db_name)
            cursor_object = db_connection.cursor()
            getting_messages_query = '''SELECT client_data FROM client_data2 '''
            cursor_object.execute(getting_messages_query)
            return cursor_object.fetchall() 
        except:
            print("Database Unavailable")
