import unittest
import sys
sys.path.append('C:\\IMQ\\')
from CLIENT import clientconnection
from CLIENT import client_message
from CLIENT import client

class ClientTesting(unittest.TestCase):
    def test_connect_to_server(self):
        message="test"
        topic="test"
        client_connection=clientconnection.ClientConnection(message,topic)
        self.assertIsNone(client_connection.connect_to_server())

if __name__ == '__main__':
    unittest.main()
    