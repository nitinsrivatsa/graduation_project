import time
import sys
sys.path.append("C:\\IMQ\\")
from CLIENT import publisher
from CLIENT import subscriber
from CLIENT import clientconnection

class Client:
    def __init__(self):
        print("Client Service")

    def main(self):
        print("Welcome To the Message Queue")
        choice=input("1: Publisher" + "\n" + "2: Subscriber" + "\n" + "Anything Else for Exit" + "\n")
        client=Client()
        
        if choice == "1":
            print("Welcome Publisher")
            print("Currently Only Football Topic is Available")
            topic="Football"
            name = input("Enter Your Name" + "\n")
            message = input("Type the message" + "\n")
            publisher_obj = publisher.Publisher(name, client)
            publisher_obj.publish(message,topic)
        
        elif choice == "2":
            print("Welcome Subscriber")
            print("Currently Only Football Topic is Available")
            topic="Football"
            message="Subscriber101"
            name = input("Enter Your Name" + "\n")
            subscriber_obj=subscriber.Subscriber(name,client)
            subscriber_obj.subscribe(message,topic)
        
        else:
            print("Thank You for Using the Message Service")
            exit

if __name__ == "__main__":
    client=Client()
    client.main()
