import sys
sys.path.append("C:\\IMQ\\")
from CLIENT import clientconnection

class Publisher:
    def __init__(self, name, client):
        self.name = name
        self.client= client
        print("Welcome " + self.name)

    def publish(self, message, topic):
        try:
             client_connections=clientconnection.ClientConnection(message,topic)
             client_connections.connect_to_server()
        except:
             print("Server is currently not available so exiting")