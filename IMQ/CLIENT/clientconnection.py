import socket
from CLIENT import constants
from CLIENT import client_message

class ClientConnection:
    def __init__(self,message,topic):
        self.host=constants.HOST
        self.port=constants.PORT
        self.message=message
        self.topic=topic

    def connect_to_server(self):
        try:
            ClientSocket = socket.socket()
            ClientSocket.connect((self.host, self.port))
            client_messages=client_message.ClientMessage(ClientSocket,self.message)
            if self.message == "Subscriber101":
                client_messages.pulling_data_from_server()
            else:
                client_messages.sending_data()
        except:
            print("Unable to Connect to Server")
