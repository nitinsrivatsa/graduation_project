from DATABASE import db_connection

class ClientMessage:
    def __init__(self,ClientSocket,message):
        self.clientsocket=ClientSocket
        self.message=message

    def sending_data(self):
        response=input("Do you want to Publish" + "\t" + "Y/N" + "\t")
        if response=="N":
            print("Client wants to Quit")
            exit
        elif response=="Y":    
            if not bool(self.message):
                print("Client wants to Quit")
                exit
            self.clientsocket.send(str.encode(self.message))
            Response = self.clientsocket.recv(1024)
            print(Response.decode('utf-8'))
            print("Message Successfully Published. Good Day!!")
        else:
            print("Taking As Yes")
            exit
        
    def pulling_data_from_server(self):
        self.clientsocket.send(str.encode(self.message))
        Response = self.clientsocket.recv(2048)
        print(Response.decode('utf-8'))

