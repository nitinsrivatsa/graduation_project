class ServerConnection(Exception):
    pass

class ServerUnavailable(ServerConnection):
    def __init__(self):
        self.message='Server is not available'