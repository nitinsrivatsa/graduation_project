class SocketException(Exception):
    pass

class ClosingConnection(SocketException):
    def __init__(self):
        self.message='Connection Unexpectedly closed by Client'

