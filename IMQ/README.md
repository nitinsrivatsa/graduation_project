IMQ is Messaging Queue system. 

Required:
Python3.x

SERVER:
Server is the engine of this Application where client can connect and either push or pull data.
Start the Server first and wait for the client to connect

CLIENT:
Client Application is the application where the client option is either a publisher or a subscriber. The topics currently available for the clients to connect to is only "Football"
Select either Publisher or Subscriber

Publisher:
Publisher connects to the Topic and publishes the data. The data is stored in the database.
Subscriber:
Subscriber connects and pulls the message.